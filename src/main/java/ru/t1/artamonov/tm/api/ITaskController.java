package ru.t1.artamonov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

}
