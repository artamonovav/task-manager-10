package ru.t1.artamonov.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

}
