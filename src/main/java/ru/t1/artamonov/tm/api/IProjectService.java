package ru.t1.artamonov.tm.api;

import ru.t1.artamonov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    void clear();

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

}
