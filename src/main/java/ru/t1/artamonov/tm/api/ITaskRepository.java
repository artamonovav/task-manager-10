package ru.t1.artamonov.tm.api;

import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clearAll();

    List<Task> findAll();

}
