package ru.t1.artamonov.tm.repository;

import ru.t1.artamonov.tm.api.ICommandRepository;
import ru.t1.artamonov.tm.constant.ArgumentConst;
import ru.t1.artamonov.tm.constant.TerminalConst;
import ru.t1.artamonov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT,
            "Display developer info."
    );

    private static final Command EXIT = new Command(TerminalConst.CMD_EXIT, null,
            "Close application."
    );

    private static final Command HELP = new Command(TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP,
            "Display list of terminal commands."
    );

    private static final Command INFO = new Command(TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO,
            "Display info."
    );

    private static final Command VERSION = new Command(TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION,
            "Display program version."
    );

    private static final Command COMMANDS = new Command(TerminalConst.CMD_COMMANDS, ArgumentConst.ARG_COMMANDS,
            "Display program commands."
    );

    private static final Command ARGUMENTS = new Command(TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARG_ARGUMENTS,
            "Display program arguments."
    );

    private static final Command PROJECT_CREATE = new Command(TerminalConst.CMD_PROJECT_CREATE, null,
            "Create new project.");

    private static final Command PROJECT_LIST = new Command(TerminalConst.CMD_PROJECT_LIST, null,
            "Display project list.");

    private static final Command PROJECT_CLEAR= new Command(TerminalConst.CMD_PROJECT_CLEAR, null,
            "Remove all projects.");

    private static final Command TASK_CREATE = new Command(TerminalConst.CMD_TASK_CREATE, null,
            "Create new task.");

    private static final Command TASK_LIST = new Command(TerminalConst.CMD_TASK_LIST, null,
            "Display task list.");

    private static final Command TASK_CLEAR = new Command(TerminalConst.CMD_TASK_CLEAR, null,
            "Remove all tasks.");

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, VERSION,
            ARGUMENTS, COMMANDS,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            HELP, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
