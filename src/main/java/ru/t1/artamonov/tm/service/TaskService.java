package ru.t1.artamonov.tm.service;

import ru.t1.artamonov.tm.api.ITaskRepository;
import ru.t1.artamonov.tm.api.ITaskService;
import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;


    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task add(final Task task) {
        if(task == null) return null;
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name) {
        if(name == null || name.isEmpty()) return null;
        return taskRepository.add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) {
        if(name == null || name.isEmpty()) return null;
        if(description == null || description.isEmpty()) return null;
        return taskRepository.add(new Task(name, description));
    }

    @Override
    public void clearAll() {
        taskRepository.clearAll();
    }

}
